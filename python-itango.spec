# Created by pyp2rpm-3.3.8
%global pypi_name itango
%global pypi_version 0.1.9

Name:           python-%{pypi_name}
Version:        %{pypi_version}
Release:        1%{?dist}.maxlab
Summary:        An interactive Tango client

License:        LGPL-3.0
URL:            https://gitlab.com/tango-controls/itango
Source0:        %{pypi_source}
BuildArch:      noarch

BuildRequires:  python3-devel
BuildRequires:  python3dist(setuptools)
BuildRequires:  python3dist(setuptools-scm)

%description
ITango is a PyTango CLI based on IPython.
It is designed to be used as an IPython profile.

%package -n     python3-%{pypi_name}
Summary:        %{summary}
%{?python_provide:%python_provide python3-%{pypi_name}}

Requires:       python3dist(pytango)
Requires:       python3dist(ipython)
Requires:       python3dist(packaging)
%description -n python3-%{pypi_name}
ITango is a PyTango CLI based on IPython.
It is designed to be used as an IPython profile.

%prep
%autosetup -n %{pypi_name}-%{pypi_version}
# Remove bundled egg-info
rm -rf %{pypi_name}.egg-info

%build
%py3_build

%install
%py3_install

%files -n python3-%{pypi_name}
%license LICENSE
%doc README.rst
%{_bindir}/itango3
%{_bindir}/itango3-qt
%{python3_sitelib}/%{pypi_name}
%{python3_sitelib}/%{pypi_name}-%{pypi_version}-py%{python3_version}.egg-info

%changelog
* Fri Jun 03 2022 Benjamin Bertrand - 0.1.9-1
- Initial package.
