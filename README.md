# itango-spec

itango SPEC file to build [itango](https://gitlab.com/tango-controls/itango) RPM.

RPM is built using [Copr] from the [PyPI itango sdist](https://pypi.org/project/itango/).

On each commit, a build is triggered in the [@tango-controls/tango-test](https://copr.fedorainfracloud.org/coprs/g/tango-controls/tango-test/) project, which is used for testing only.

RPM in the [@tango-controls/tango](https://copr.fedorainfracloud.org/coprs/g/tango-controls/tango/) project is only built when pushing a tag.

## Installing the RPMs

[Copr] can be used as a repository but only the latest build is kept forever.
To install the latest version of itango from [Copr], you can run:

* on CentOS 8

    ```bash
    dnf install -y epel-release 'dnf-command(copr)'
    dnf copr -y enable @tango-controls/tango
    # Enabling powertools is required to install boost-python3
    dnf config-manager --set-enabled powertools
    # You can now install itango
    dnf install -y python3-itango
    ```

RPMs are still available from MAX IV public repository.
Refer to the Tango documentation to [install Tango on CentOS](https://tango-controls.readthedocs.io/en/latest/installation/tango-on-linux.html#centos).

## Making a new release

RPMs on [Copr] aren't immutable. [Copr] will overwrite previous RPMs if you submit a new build without changing the version.

You should **never tag without changing the version or release** number in the `python-itango.spec` file.

To create a new release:

* Increase the `pypi_version` number in `python-itango.spec`. Reset the `Release` to 1.
* If the version is identical to the previous RPM, you have to increase the `Release` number.
* Update the requirements if needed.
* Push your changes to the git repository.
* Wait for the pipeline to finish. Build and test should be successful.
* You can perform extra tests using the RPMs from `@tango-controls/tango-test`.
* To make a new release, tag the branch (`git tag -a <itango version>-<release>`) and push the tag. This will trigger the build in the `@tango-controls/tango` project.

Note that the tag itself isn't used for the RPMS version. It is used as version to upload the source RPM to GitLab as a generic package.
See [GitLab documentation on generic package](https://docs.gitlab.com/ee/user/packages/generic_packages/#publish-a-package-file).

To trigger the build in [Copr], [copr-cli](https://developer.fedoraproject.org/deployment/copr/copr-cli.html) is used. The configuration is saved under [GitLab CI/CD RPM subgroup settings] as a variable. **The token is only valid for 180 days.**
When expired, it needs to be replaced. To get a new token:

* login to [Copr] as `tangocontrolsbot`
* go to the [Copr API](https://copr.fedorainfracloud.org/api/) page
* click on *Generate a new token*
* copy/paste the configuration to the `COPR_CONFIG` variable in [GitLab CI/CD RPM subgroup settings]

[Copr]: https://copr.fedorainfracloud.org
[GitLab CI/CD RPM subgroup settings]: https://gitlab.com/groups/tango-controls/RPM/-/settings/ci_cd
